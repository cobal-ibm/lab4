       IDENTIFICATION DIVISION.
       PROGRAM-ID. BMICALC.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
           01 HEIGHT-IN-Cen          PIC 999.
           01 HEIGHT-IN-METERS       PIC 9V99.
           01 HEIGHT-IN-METERS2      PIC 9V99.
           01 WEIGHT-IN-KILOGRAMS    PIC 99.
           01 BMI                    PIC 99V99.
           01 BMI-CATEGORY           PIC X(20).

       PROCEDURE DIVISION.

           DISPLAY "BMI CALCULATOR".
           DISPLAY "=================".
           DISPLAY "Enter your height in Centimeter (cm): ".
           ACCEPT HEIGHT-IN-Cen.

           DISPLAY "Enter your weight in kilograms (kg): ".
           ACCEPT WEIGHT-IN-KILOGRAMS.
           
           COMPUTE HEIGHT-IN-METERS = HEIGHT-IN-Cen / 100
           COMPUTE HEIGHT-IN-METERS2 = HEIGHT-IN-METERS * 
                                       HEIGHT-IN-METERS

           
           COMPUTE BMI = WEIGHT-IN-KILOGRAMS / HEIGHT-IN-METERS2
                          

           IF BMI < 18.5
              MOVE "Underweight" TO BMI-CATEGORY
           ELSE IF BMI >= 18.5 AND BMI < 24.9
              MOVE "Normal Weight" TO BMI-CATEGORY
           ELSE IF BMI >= 25 AND BMI < 29.9
              MOVE "Overweight" TO BMI-CATEGORY
           ELSE
              MOVE "Obese" TO BMI-CATEGORY
           END-IF
           .

           DISPLAY "Your BMI is: " BMI
           DISPLAY "BMI Category: " BMI-CATEGORY.
           
           GOBACK
           .
